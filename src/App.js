import React, { Component, Fragment } from 'react';
import logo from './logo.svg';
import './App.css';

import * as $ from 'jquery';

const CHART = {
	HEIGHT: 500
}

class App extends Component {

	data = [
		{color: 'red', height: 100},
		{color: 'blue', height: 150},
		{color: 'green', height: 90},
		{color: 'orange', height: 200},
		{color: 'brown ', height: 300},
		{color: 'DeepPink', height: 300},
		{color: 'LightCoral', height: 250}
	];

	renderColumn = (style = {fill: 'blue', stroke: 'red', strokeWidth: 1,}, {x, height}) => {

		let width = 50;
		let y = CHART.HEIGHT - height;
		let textX = x + (width - 30)/2;

		return (
			<g style={style} onMouseMove={this.onMouseMove} onMouseLeave={this.onMouseLeave}>
				<rect x={x} y={y} width={width} height={height}
								/>
				<text x={textX} y={y - 5} fill={style.fill}>{height}</text>
			</g>
		)
	}

	onMouseMove = (e) => {
		
		let $target = $(e.target).closest('g');
		$target.css({
			'fill-opacity': 0.4
		});
	}

	onMouseLeave = (e) => {
		let $target = $(e.target).closest('g');
		$target.css({
			'fill-opacity': 1
		});
	}

	render() {

		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Build a chart by svg</h1>
				</header>
				<p className="App-intro">
					<svg width="800" height={CHART.HEIGHT} style={{backgroundColor: '#ddd'}}>
						{
							this.data.map((item, index)=> {
								return <Fragment key={index}>{this.renderColumn({fill: item.color}, {x: 50 * index, height: item.height})}</Fragment>
							})
						}
						
					</svg>
				</p>
			</div>
		);
	}
}

export default App;
